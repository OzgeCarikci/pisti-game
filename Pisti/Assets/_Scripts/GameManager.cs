﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton
    private static GameManager mInstance;

    public static GameManager Instance
    {
        get
        {
            if (mInstance != null)
                return mInstance;

            mInstance = FindObjectOfType(typeof(GameManager)) as GameManager;

            if (mInstance != null)
                return mInstance;

            var cont = new GameObject("GameManager");
            mInstance = cont.AddComponent<GameManager>();
            return mInstance;
        }
    }
    #endregion

    public UIManager UIManager;
    public SoundManager SoundManager;
    public TableManager TableManager;
    public ScoreManager ScoreManager;

    public event Action OnInitializedGame;
    public event Action OnStartGame;
    public event Action OnFinishGame;
    public event Action OnRestartGame;
    public event Action OnFinishTurn;

    public event Action OnTurnChange;

    public UserTypes CurrentTurn;

    private void Awake()
    {
        Application.targetFrameRate = 40;
        UIManager.Initialize(this);
        SoundManager.Initialize(this);
        TableManager.Initialize(this);
        ScoreManager.Initialize(this);

        if (OnInitializedGame != null)
        {
            OnInitializedGame();
        }
    }

    public void StartGame()
    {
        if (OnStartGame != null)
        {
            OnStartGame();
        }
    }

    public void FinishGame()
    {
        if (OnFinishGame != null)
        {
            OnFinishGame();
        }
    }

    public void RestartGame()
    {
        if (OnRestartGame != null)
        {
            OnRestartGame();
        }
    }

    public void FinishTurn()
    {
        if(OnFinishTurn != null)
        {
            OnFinishTurn();
        }
    }

    public void ChangeTurn()
    {
        CurrentTurn = CurrentTurn == UserTypes.AI ? UserTypes.Player : UserTypes.AI;
        if(OnTurnChange != null)
        {
            OnTurnChange();
        }


    }
}
