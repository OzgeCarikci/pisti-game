﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : PistiBehaviour
{
    public AudioSource ClickAudioSource;
    public AudioClip[] ClickAudioClips;

    [Space(20)]
    public AudioSource TableAudioSource;
    public AudioClip[] TableAudioClips;

    [Space(20)]
    public AudioSource PistiAudioSource;

    public override void Initialize(GameManager gameManager)
    {
        base.Initialize(gameManager);
    }

    public void PlayClickSound(ClickSounds clickSound)
    {
        ClickAudioSource.clip = ClickAudioClips[(int)clickSound];
        ClickAudioSource.Play();
    }

    public void PlayTableSound(TableSounds sound)
    {
        TableAudioSource.clip = TableAudioClips[(int)sound];
        TableAudioSource.Play();
    }

    public void PlayPistiSound()
    {
        PistiAudioSource.Play();
    }
}
