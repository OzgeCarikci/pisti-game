﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Users
{
    public List<Card> Hand { get; set; }
    public List<Card> CollectedCards { get; set; }

    public int Score { get; set; }

    public bool CanCollect = false;
    public List<Card> PistiCards = new List<Card>();

    public UserTypes UserType { get; set; }

    public Users()
    {
        GameManager.Instance.OnTurnChange += OnTurnChange;
        GameManager.Instance.OnFinishTurn += OnFinishTurn;
    }

    public virtual void OnTurnChange()
    {
        if (Hand.Count == 0 && GameManager.Instance.CurrentTurn == this.UserType)
        {
            GameManager.Instance.TableManager.UpdateHand();
        }
    }

    public virtual void OnFinishTurn()
    {
        Score += GameManager.Instance.ScoreManager.CalculateScore(CollectedCards, PistiCards);
    }

    public virtual void DropCard(Card card)
    {
        CanCollect = false;

        GameManager.Instance.SoundManager.PlayClickSound(ClickSounds.Card);

        var tableCard = GameManager.Instance.TableManager.TableCards.LastOrDefault();
        var tableCardCount = GameManager.Instance.TableManager.TableCards.Count;

        GameManager.Instance.TableManager.AddToTableCard(card);

        if (tableCard != null)
        {
            if (tableCard.CardId == card.CardId)
            {
                if (tableCardCount == 1)
                {
                    Pisti(card);
                }

                CanCollect = true;

            }
            else if (card.CardId == Constants.JOKER_CARD_NUMBER)
            {
                CanCollect = true;
            }
        }

        Hand.Remove(card);

        if (CanCollect)
        {
            GameManager.Instance.TableManager.CollectCard(this);
        }
    }

    public virtual void AddToHand(List<Card> cards)
    {
        Hand.AddRange(cards);
    }

    public virtual void AddToCollectedCards(List<Card> cards)
    {
        this.CollectedCards.AddRange(cards);
    }

    public virtual void AddToScore(int val)
    {
        this.Score += val;
    }

    private void Pisti(Card card)
    {
        GameManager.Instance.SoundManager.PlayPistiSound();
        this.PistiCards.Add(card);
    }

    private void OnDestroy()
    {
        GameManager.Instance.OnTurnChange -= OnTurnChange;
        GameManager.Instance.OnFinishTurn -= OnFinishTurn;
    }
}
