﻿public enum Panels
{
    MainMenu = 0,
    Hud = 1,
    Pause = 2,
    FinishGame = 3
}

public enum CardTypes
{
    Clubs = 0,
    Spades = 1,
    Hearts = 2,
    Diamonds = 3,

    MAX = 4,
    None = 5
}

public enum CardStates
{
    Open = 0,
    Closed = 1,
    Inactive = 2
}

public enum UICardTypes : int
{
    Player = 0,
    AI = 1,
    Table = 2
}

public enum ClickSounds
{
    Button,
    Card
}

public enum TableSounds
{
    ShuffleCards = 0,
    WinGame = 1,
    LostGame = 2,
    DealGame = 3
}

public enum UserTypes
{
    Player = 0,
    AI = 1
}