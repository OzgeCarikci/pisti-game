﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AI : Users
{
    public AI()
    {
        Hand = new List<Card>();
        CollectedCards = new List<Card>();
        UserType = UserTypes.AI;
    }

    public override void AddToHand(List<Card> cards)
    {
        base.AddToHand(cards);
    }

    public override void DropCard(Card card)
    {
        base.DropCard(card);

        GameManager.Instance.TableManager.HudPanel.UICardManagers[(int)UICardTypes.AI].UICards.Where(x => x.Card.Equals(card)).FirstOrDefault().UpdateCard(card, CardStates.Inactive);

        GameManager.Instance.ChangeTurn();

    }

    public override void OnTurnChange()
    {
        base.OnTurnChange();
        if (GameManager.Instance.CurrentTurn == UserTypes.AI)
        {
            DropCard(GetRandomCard());
        }
    }

    public override void AddToCollectedCards(List<Card> cards)
    {
        base.AddToCollectedCards(cards);
        GameManager.Instance.TableManager.HudPanel.AiCollectedCard.UpdateCard(cards.LastOrDefault(), CardStates.Open);
    }

    public override void OnFinishTurn()
    {
        base.OnFinishTurn();
        ((HudPanel)GameManager.Instance.UIManager.UIPanels[(int)Panels.Hud]).UpdateAiScoreText(Score);
    }

    private Card GetRandomCard()
    {
        var tableCard = GameManager.Instance.TableManager.TableCards.LastOrDefault();
        var tableCardCount = GameManager.Instance.TableManager.TableCards.Count;

        if (tableCardCount > 0)
        {
            var sameCardIndex = Hand.Includes(tableCard);
            var jokerCardIndex = Hand.Includes(Constants.JOKER_CARD_NUMBER);

            if (sameCardIndex != null)
            {
                return Hand[(int)sameCardIndex];
            }
            else if (jokerCardIndex != null)
            {
                return Hand[(int)jokerCardIndex];
            }
        }

        return Hand[UnityEngine.Random.Range(0, Hand.Count)];
    }
}
