﻿public class Constants
{
    public const int MAX_GAME_COUNT = 3;
    public const int IN_HAND_MAX_CARD_COUNT = 4;
    public const int MAX_CARD_COUNT = 13;
    public const int DECK_CARD_COUNT = 52;
    public const int JOKER_CARD_NUMBER = 11;

    public const int CARD_BONUS_POINT = 3;
    public const int CLUBS_TWO_POINT = 2;
    public const int DIAMONDS_TEN_POINT = 3;
    public const int PISTI_POINT = 10;
    public const int JOKER_PISTI_COUNT = 20;

    public const string CARD_NAME = "tex{0}{1}"; // 0 : cardType , 1 : cardNumber
    public const string DECK_CARD_COUNT_TEXT = "Deck[{0}]";
    public const string PLAYER_SCORE_TEXT = "Player[{0}]";
    public const string AI_SCORE_TEXT = "AI[{0}]";
    public const string GAME_COUNT_TEXT = "{0}. Turn";

    public const string DEAL_TEXT = "DEAL!";
    public const string WIN_TEXT = "YOU WIN!";
    public const string LOST_TEXT = "COMPUTER WIN!";
}
