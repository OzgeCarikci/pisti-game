﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TableManager : PistiBehaviour
{
    public Card[] Deck { get; set; }

    public List<Users> Users { get; set; }

    private List<Card> mAvailableCards = new List<Card>();
    public List<Card> TableCards { get; set; }

    public UserTypes LastCollectedUser { get; set; }

    public int PlayedGameCount { get; set; }

    public Card ClubTwo = new Card(CardTypes.Clubs, null, 2);
    public Card DiamondTen = new Card(CardTypes.Diamonds, null, 10);
    public Card A = new Card(CardTypes.None, null, 1);
    public Card Joker = new Card(CardTypes.None, null, 11);


    public HudPanel HudPanel
    {
        get
        {
            return ((HudPanel)GameManager.UIManager.GetPanel(Panels.Hud));
        }
    }

    public override void Initialize(GameManager gameManager)
    {
        base.Initialize(gameManager);

        GameManager.OnInitializedGame += OnInitializedGame;
        GameManager.OnStartGame += OnStartGame;
        GameManager.OnFinishGame += OnFinishGame;
        GameManager.OnRestartGame += OnRestartGame;
    }

    private void OnInitializedGame()
    {
        InitTable();
    }

    private void OnStartGame()
    {
        DealTable();
        DealCards();
    }

    private void OnFinishGame()
    {
        if (TableCards.Count > 0)
        {
            CollectCard(Users[(int)LastCollectedUser]);
        }
    }

    private void OnRestartGame()
    {
        PlayedGameCount = 0;
        Users.ForEach(x => { x.CollectedCards.Clear(); x.PistiCards.Clear(); x.Hand.Clear(); });
        Users.ForEach(x => x.Score = 0);
        StartTurn();
        DealTable();
        DealCards();

        HudPanel.PlayerCollectedCard.UpdateState(CardStates.Closed);
        HudPanel.AiCollectedCard.UpdateState(CardStates.Closed);
    }

    private void InitTable()
    {
        Users = new List<Users>();
        Users.Add(new Player());
        Users.Add(new AI());
        Deck = new Card[Constants.DECK_CARD_COUNT];

        var cardIndex = 0;

        for (int typeNum = 0; typeNum < (int)CardTypes.MAX; typeNum++)
        {
            for (int cardNum = 1; cardNum <= Constants.MAX_CARD_COUNT; cardNum++)
            {
                var cardType = (CardTypes)typeNum;
                var cardSpriteName = string.Format(Constants.CARD_NAME, cardType, cardNum);
                var cardSprite = Resources.Load<Sprite>(cardSpriteName);

                Deck[cardIndex++] = new Card(cardType, cardSprite, cardNum);
            }
        }

        StartTurn();
    }

    private void DealCards()
    {
        GameManager.SoundManager.PlayTableSound(TableSounds.ShuffleCards);

        var playerCards = GetRandomCards(Constants.IN_HAND_MAX_CARD_COUNT);
        var aiCards = GetRandomCards(Constants.IN_HAND_MAX_CARD_COUNT);

        Users[(int)UserTypes.Player].AddToHand(playerCards);
        Users[(int)UserTypes.AI].AddToHand(aiCards);

        int index = 0;

        Users[(int)UserTypes.Player].Hand.ForEach(card => SetUICard(card, (int)UICardTypes.Player, CardStates.Open, index++));

        index = 0;
        Users[(int)UserTypes.AI].Hand.ForEach(card => SetUICard(card, (int)UICardTypes.AI, CardStates.Closed, index++));

        HudPanel.UpdateDeckCountText(mAvailableCards.Count);
    }

    private void DealTable()
    {
        TableCards = GetRandomCards(Constants.IN_HAND_MAX_CARD_COUNT);
        HudPanel.UICardManagers[(int)UICardTypes.Table].UICards.FirstOrDefault().UpdateCard(TableCards.LastOrDefault(), CardStates.Open);
        HudPanel.UICardManagers[(int)UICardTypes.Table].UICards.LastOrDefault().UpdateState(CardStates.Closed);
    }

    public void UpdateHand()
    {
        if (mAvailableCards.Count == 0)
        {
            GameManager.Instance.FinishTurn();

            if (PlayedGameCount >= Constants.MAX_GAME_COUNT)
            {
                GameManager.Instance.FinishGame();
            }
            else
            {
                NextTurn();
            }
        }
        else
        {
            DealCards();
        }
    }

    private void StartTurn()
    {
        PlayedGameCount++;
        mAvailableCards = Deck.ToList();
        mAvailableCards.Shuffle();
        HudPanel.UpdateGameCountText(PlayedGameCount);
    }

    private void NextTurn()
    {
        StartTurn();
        DealCards();
        DealTable();

        HudPanel.PlayerCollectedCard.UpdateState(CardStates.Closed);
        HudPanel.AiCollectedCard.UpdateState(CardStates.Closed);

        Users.ForEach(x => { x.CollectedCards.Clear(); x.PistiCards.Clear(); });
    }

    private void SetUICard(Card card, int uiCardType, CardStates state, int index)
    {
        HudPanel.UICardManagers[uiCardType].UICards[index].UpdateCard(card, state);
    }

    private List<Card> GetRandomCards(int count)
    {
        var randomCards = mAvailableCards.Take(count).ToList();
        mAvailableCards.RemoveRange(0, count);

        return randomCards;
    }

    private void OnDestroy()
    {
        if (GameManager != null)
        {
            GameManager.OnInitializedGame -= OnInitializedGame;
            GameManager.OnStartGame -= OnStartGame;
            GameManager.OnFinishGame -= OnFinishGame;
            GameManager.OnRestartGame -= OnRestartGame;
        }
    }

    public void AddToTableCard(Card card)
    {
        GameManager.Instance.TableManager.TableCards.Add(card);
        HudPanel.UICardManagers[(int)UICardTypes.Table].UICards.FirstOrDefault().UpdateCard(card, CardStates.Open);
    }

    public void CollectCard(Users user)
    {
        LastCollectedUser = user.UserType;
        user.AddToCollectedCards(TableCards);
        TableCards.Clear();

        HudPanel.UICardManagers[(int)UICardTypes.Table].UICards.ForEach(x => x.UpdateCard(null, CardStates.Inactive));

    }

    public bool IsDeal()
    {
        return Users[(int)UserTypes.Player].Score == Users[(int)UserTypes.AI].Score;
    }

    public UserTypes Winner()
    {
        var maxScore = Users.Max(x => x.Score);
        return Users.FirstOrDefault(x => x.Score == maxScore).UserType;
    }
}
