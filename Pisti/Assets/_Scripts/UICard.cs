﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UICard : PistiBehaviour, IPointerClickHandler
{
    public Card Card { get; set; }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (GameManager.Instance.CurrentTurn == UserTypes.Player)
        {
            UpdateState(CardStates.Inactive);
            GameManager.Instance.TableManager.Users[(int)UserTypes.Player].DropCard(Card);
        }
    }

    public void UpdateCard(Card card, CardStates state)
    {
        Card = card;
        UpdateState(state);
    }

    public void UpdateState(CardStates state)
    {
        gameObject.SetActive(state == CardStates.Inactive ? false : true);

        if (state == CardStates.Open)
        {
            Image.sprite = Card.CardTexture;
        }
        else if (state == CardStates.Closed)
        {
            Image.sprite = ((HudPanel)GameManager.Instance.UIManager.UIPanels[(int)Panels.Hud]).ClosedCardSprite;
        }
    }
}
