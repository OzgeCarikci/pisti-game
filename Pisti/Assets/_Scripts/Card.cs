﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card
{
    public CardTypes CardType { get; set; }

    public Sprite CardTexture { get; set; }

    public int CardId { get; set; }

    public Card(CardTypes type, Sprite sprite, int cardId)
    {
        CardType = type;
        CardTexture = sprite;
        CardId = cardId;
    }

    public override bool Equals(object obj)
    {
        var otherCard = (Card)obj;
        return this.CardId == otherCard.CardId && this.CardType == otherCard.CardType;
    }
} 
