﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScoreManager : PistiBehaviour
{
    public override void Initialize(GameManager gameManager)
    {
        base.Initialize(gameManager);

        GameManager.OnFinishGame += OnFinishGame;
    }

    private void OnFinishGame()
    {

    }

    public int CalculateScore(List<Card> collectedCards, List<Card> pistiCards)
    {
        int score = 0;
        if (collectedCards != null && collectedCards.Count > 0)
        {
            int bonus = 0;
            if (collectedCards.Count > (Constants.DECK_CARD_COUNT / 2))
            {
                bonus = Constants.CARD_BONUS_POINT;
            }

            int clubTwoPoint = collectedCards.FirstOrDefault(x => x.Equals(GameManager.TableManager.ClubTwo)) != null ? Constants.CLUBS_TWO_POINT : 0;

            int diamondTenPoint = collectedCards.FirstOrDefault(x => x.Equals(GameManager.TableManager.DiamondTen)) != null ? Constants.DIAMONDS_TEN_POINT : 0;

            int aPoint = collectedCards.Count(x => x.CardId == GameManager.TableManager.A.CardId);
            int jokerPoint = collectedCards.Count(x => x.CardId == GameManager.TableManager.Joker.CardId);


            score = bonus + clubTwoPoint + diamondTenPoint + aPoint + jokerPoint;
        }

        if (pistiCards != null && pistiCards.Count > 0)
        {
            int pistiPoint = pistiCards.Count(x => x.CardId != Constants.JOKER_CARD_NUMBER) * Constants.PISTI_POINT;
            int jokerPistiPoint = pistiCards.Count(x => x.CardId == Constants.JOKER_CARD_NUMBER) * Constants.JOKER_PISTI_COUNT;

            score += pistiPoint + jokerPistiPoint;
        }

        return score;
    }

    public void OnDestroy()
    {
        if (GameManager != null)
        {
            GameManager.OnFinishGame -= OnFinishGame;
        }
    }
}
