﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FinishGamePanel : UIPanel
{
    public CustomButton RestartButton;

    public Text WinStateText;
    public Text PlayerScoreText;
    public Text AiScoreText;

    public override void Initialize(UIManager uiManager)
    {
        base.Initialize(uiManager);

        RestartButton.Initialize(uiManager, OnButtonClickedRestart);
        GameManager.OnFinishGame += OnFinishGame;
    }

    private void OnFinishGame()
    {
        ShowPanel();

        PlayerScoreText.text = GameManager.TableManager.Users[(int)UserTypes.Player].Score.ToString();
        AiScoreText.text = GameManager.TableManager.Users[(int)UserTypes.AI].Score.ToString();

        if (UIManager.GameManager.TableManager.IsDeal())
        {
            WinStateText.text = Constants.DEAL_TEXT;
            UIManager.GameManager.SoundManager.PlayTableSound(TableSounds.DealGame);
        }
        else
        {
            switch (UIManager.GameManager.TableManager.Winner())
            {
                case UserTypes.Player:
                    WinStateText.text = Constants.WIN_TEXT;
                    UIManager.GameManager.SoundManager.PlayTableSound(TableSounds.WinGame);

                    break;

                case UserTypes.AI:
                    WinStateText.text = Constants.LOST_TEXT;
                    UIManager.GameManager.SoundManager.PlayTableSound(TableSounds.LostGame);
                    break;
            }
        }
    }

    private void OnButtonClickedRestart()
    {
        UIManager.GameManager.SoundManager.PlayClickSound(ClickSounds.Button);
        GameManager.RestartGame();
        HidePanel();
    
    }

    private void OnDestroy()
    {
        if (GameManager != null)
        {
            GameManager.OnFinishGame -= OnFinishGame;
        }
    }
}
