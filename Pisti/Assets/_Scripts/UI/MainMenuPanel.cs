﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenuPanel : UIPanel
{
    public CustomButton StartButton;

    public override void Initialize(UIManager uiManager)
    {
        base.Initialize(uiManager);

        StartButton.Initialize(uiManager, OnButtonClickedStart);
    }

    private void OnButtonClickedStart()
    {
        GameManager.StartGame();
        UIManager.GameManager.SoundManager.PlayClickSound(ClickSounds.Button);
        UIManager.GetPanel(Panels.Hud).ShowPanel();
    }
}
