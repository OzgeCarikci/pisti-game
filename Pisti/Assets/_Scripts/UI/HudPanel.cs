﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HudPanel : UIPanel
{
    public CustomButton PauseButton;
    public Sprite ClosedCardSprite;

    public List<UICardManager> UICardManagers;

    public UICard PlayerCollectedCard;
    public UICard AiCollectedCard;

    public Text PlayerScoreText;
    public Text AiScoreText;
    public Text DeckCountText;
    public Text GameCountText;

    public override void Initialize(UIManager uiManager)
    {
        base.Initialize(uiManager);
        PauseButton.Initialize(UIManager, OnButtonClickedPause);
        UIManager.GameManager.OnRestartGame += OnRestartGame;
    }

    private void OnRestartGame()
    {
        UpdatePlayerScoreText(0);
        UpdateAiScoreText(0);
        UpdateGameCountText(1);
    }

    private void OnButtonClickedPause()
    {
        UIManager.GameManager.SoundManager.PlayClickSound(ClickSounds.Button);
        UIManager.GetPanel(Panels.Pause).ShowPanel();
    }

    public void UpdatePlayerScoreText(int score)
    {
        PlayerScoreText.text = string.Format(Constants.PLAYER_SCORE_TEXT, score);
    }

    public void UpdateAiScoreText(int score)
    {
        AiScoreText.text = string.Format(Constants.AI_SCORE_TEXT, score);
    }

    public void UpdateDeckCountText(int score)
    {
        DeckCountText.text = string.Format(Constants.DECK_CARD_COUNT_TEXT, score);
    }

    public void UpdateGameCountText(int count)
    {
        GameCountText.text = string.Format(Constants.GAME_COUNT_TEXT, count);
    }

    private void OnDestroy()
    {
        if (UIManager != null && UIManager.GameManager != null)
        {
            UIManager.GameManager.OnRestartGame -= OnRestartGame;
        }
    }
}
