﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CustomButton : Button
{
    #region Fields

    public bool DontPlayAnimation { get; set; }

    protected UIManager UIManager;

    private event Action mOnClick;

    private event Action<PointerEventData> mOnClickWithData;
    private event Action<PointerEventData> mOnPointerDown;
    private event Action<PointerEventData> mOnPointerUp;

    protected bool mHasInteraction;
    public bool HasInteraction { get { return mHasInteraction; } }

    private Vector3 mStartScale;

    #endregion

    #region Methods

    public virtual void Initialize(UIManager uiManager, Action<PointerEventData> onClickWithData, Action<PointerEventData> onPointerDown, Action<PointerEventData> onPointerUp, bool active = true)
    {
        gameObject.SetActive(active);
        UIManager = uiManager;
        mOnClickWithData = onClickWithData;
        mOnPointerDown = onPointerDown;
        mOnPointerUp = onPointerUp;

        mStartScale = transform.localScale;

        if (transform.localScale.x < 0.1f)
        {
            mStartScale = Vector3.one;
        }
    }

    public virtual void Initialize(UIManager uiManager, Action onClick, bool active = true)
    {
        gameObject.SetActive(active);
        UIManager = uiManager;
        mOnClick = onClick;

        mStartScale = transform.localScale;

        if (transform.localScale.x < 0.1f)
        {
            mStartScale = Vector3.one;
        }
    }

    public virtual void Initialize(UIManager uiManager, Action onClick, bool noAnimation, bool active)
    {
        Initialize(uiManager, onClick, true);
        DontPlayAnimation = noAnimation;
    }

    public virtual void SetInteraction(bool interaction)
    {
        interactable = interaction;
        image.color = interaction ? Color.white : Color.gray;
        mHasInteraction = interaction;
    }

    #endregion

    #region Events

    public override void OnPointerClick(PointerEventData eventData)
    {
        if (!interactable)
            return;

        if (eventData != null)
        {
            base.OnPointerClick(eventData);
        }

        if (mOnClick != null)
        {
            mOnClick();
        }

        if (eventData != null)
        {
            if (mOnClickWithData != null)
                mOnClickWithData(eventData);
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        if (!interactable)
            return;

        if (!DontPlayAnimation && UIManager != null)
        {
            transform.localScale = new Vector3(mStartScale.x - .1f, mStartScale.y - .1f, mStartScale.z - .1f);
        }

        base.OnPointerDown(eventData);

        if (mOnPointerDown != null)
            mOnPointerDown(eventData);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        if (!interactable)
            return;

        if (!DontPlayAnimation && UIManager != null)
            transform.localScale = mStartScale;

        base.OnPointerUp(eventData);

        if (mOnPointerUp != null)
            mOnPointerUp(eventData);
    }

    #endregion
}
