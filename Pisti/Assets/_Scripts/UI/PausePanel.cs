﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PausePanel : UIPanel
{
    public CustomButton ResumeButton;
    public CustomButton RestartButton;

    public override void Initialize(UIManager uiManager)
    {
        base.Initialize(uiManager);

        ResumeButton.Initialize(uiManager, OnButtonClickedResume);
        RestartButton.Initialize(uiManager, OnButtonClickedRestart);
    }

    private void OnButtonClickedRestart()
    {
        UIManager.GameManager.SoundManager.PlayClickSound(ClickSounds.Button);
        GameManager.RestartGame();
        HidePanel();

    }

    private void OnButtonClickedResume()
    {
        UIManager.GameManager.SoundManager.PlayClickSound(ClickSounds.Button);
        UIManager.GetPanel(Panels.Pause).HidePanel();
    }
}
