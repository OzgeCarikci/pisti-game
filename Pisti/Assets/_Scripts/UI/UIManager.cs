﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : PistiBehaviour
{
    public UIPanel CurrentUIPanel { get; set; }

    public List<UIPanel> UIPanels;

    //public Card

    public override void Initialize(GameManager gameManager)
    {
        base.Initialize(gameManager);
        gameManager.OnStartGame += OnStartGame;

        UIPanels.ForEach(o =>
        {
            o.Initialize(this);
            o.gameObject.SetActive(false);
        });

        UIPanels[(int)Panels.MainMenu].ShowPanel();
    }

    private void Update()
    {
        UpdateEscapeButton();
    }

    private void OnStartGame()
    {
        GetPanel(Panels.Hud).ShowPanel();
    }

    private void UpdateEscapeButton()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CurrentUIPanel.EscapeClicked();
        }
    }

    public void SetCurrentUIPanel(UIPanel panel)
    {
        CurrentUIPanel = panel;
    }

    public void SetCurrentUIPanel(Panels panel)
    {
        CurrentUIPanel = GetPanel(panel);
    }

    public UIPanel GetPanel(Panels panel)
    {
        return UIPanels[(int)panel];
    }

    private void OnDestroy()
    {
        if (GameManager != null)
            GameManager.OnStartGame -= OnStartGame;
    }
}
