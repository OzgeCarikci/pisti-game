﻿using UnityEngine;
using UnityEngine.UI;

public class PistiBehaviour : MonoBehaviour
{
    public GameManager GameManager { get; set; }

    private LayoutElement mLayoutElement;
    public LayoutElement LayoutElement
    {
        get
        {
            if (mLayoutElement == null)
            {
                mLayoutElement = base.GetComponent<LayoutElement>();
            }

            return mLayoutElement;
        }
    }

    private Transform mTransform;
    public Transform Transform
    {
        get
        {
            if (mTransform == null)
            {
                mTransform = base.transform;
            }

            return mTransform;
        }
    }

    private Animator mAnimator;
    public Animator Animator
    {
        get
        {
            if (mAnimator == null)
            {
                mAnimator = base.GetComponent<Animator>();
            }

            return mAnimator;
        }
    }

    private Collider mCollider;

    public Collider Collider
    {
        get
        {
            if (mCollider == null)
            {
                mCollider = base.GetComponent<Collider>();
            }
            return mCollider;
        }
    }

    private Animation mAnimation;

    public Animation Animation
    {
        get
        {
            if (mAnimation == null)
            {
                mAnimation = base.GetComponent<Animation>();
            }
            return mAnimation;
        }
    }

    private RectTransform mRectTransform;
    public RectTransform RectTransform
    {
        get
        {
            if (mRectTransform == null)
            {
                mRectTransform = base.GetComponent<RectTransform>();
            }

            return mRectTransform;
        }
    }

    private Rigidbody2D rigidBody;
    public Rigidbody2D RigidBody
    {
        get
        {
            if (rigidBody == null)
            {
                rigidBody = base.GetComponent<Rigidbody2D>();
            }

            return rigidBody;
        }
    }

    private CanvasGroup mCanvasGroup;
    public CanvasGroup CanvasGroup
    {
        get
        {
            if (mCanvasGroup == null)
            {
                mCanvasGroup = base.GetComponent<CanvasGroup>();
            }

            return mCanvasGroup;
        }
    }

    private Text mText;
    public Text Text
    {
        get
        {
            if (mText == null)
            {
                mText = base.GetComponent<Text>();
            }

            return mText;
        }
    }

    private Text mChildText;
    public Text ChildText
    {
        get
        {
            if (mChildText == null)
            {
                mChildText = base.GetComponentInChildren<Text>();
            }

            return mChildText;
        }
    }

    private Camera mCamera;

    public Camera Camera
    {
        get
        {
            if (mCamera == null)
            {
                mCamera = base.GetComponent<Camera>();
            }

            return mCamera;
        }
    }
    private Image mImage;

    public Image Image
    {
        get
        {
            if (mImage == null)
            {
                mImage = base.GetComponent<Image>();
            }

            return mImage;
        }
    }

    public virtual void Initialize(GameManager gameManager)
    {
        GameManager = gameManager;
    }
}
