﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Includes extension methods etc.
/// </summary>
public static class PistiUtility
{
    public static void Open(this CanvasGroup canvas)
    {
        canvas.alpha = 1;
        canvas.blocksRaycasts = true;
        canvas.interactable = true;
    }

    public static void Close(this CanvasGroup canvas)
    {
        canvas.alpha = 0;
        canvas.blocksRaycasts = false;
        canvas.interactable = false;
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        int listCount = list.Count;

        var rnd = new System.Random();

        while (listCount > 1)
        {
            int val = (rnd.Next(0, listCount) % listCount);
            listCount--;

            T value = list[val];
            list[val] = list[listCount];
            list[listCount] = value;
        }
    }

    public static int? Includes(this List<Card> cards, Card card)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].CardId == card.CardId)
            {
                return i;
            }
        }

        return null;
    }

    public static int? Includes(this List<Card> cards, int cardId)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].CardId == cardId)
            {
                return i;
            }
        }

        return null;
    }
}
