﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : Users
{
    public Player()
    {
        Hand = new List<Card>();
        CollectedCards = new List<Card>();
        UserType = UserTypes.Player;
    }

    public override void AddToHand(List<Card> cards)
    {
        base.AddToHand(cards);
    }

    public override void DropCard(Card card)
    {
        base.DropCard(card);
        GameManager.Instance.ChangeTurn();
    }

    public override void AddToCollectedCards(List<Card> cards)
    {
        base.AddToCollectedCards(cards);

        ((HudPanel)GameManager.Instance.UIManager.UIPanels[(int)Panels.Hud]).PlayerCollectedCard.UpdateCard(cards.LastOrDefault(), CardStates.Open);
    }

    public override void OnFinishTurn()
    {
        base.OnFinishTurn();
        ((HudPanel)GameManager.Instance.UIManager.UIPanels[(int)Panels.Hud]).UpdatePlayerScoreText(Score);
    }
}
